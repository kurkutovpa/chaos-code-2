﻿class Rectangle {

    left: number;
    top: number;
    width: number;
    height: number;
    right: number;
    bottom: number;


    constructor(left, top, width, height) {
        this.left = left || 0;
        this.top = top || 0;
        this.width = width || 0;
        this.height = height || 0;
        this.right = this.left + this.width;
        this.bottom = this.top + this.height;
    }

    setValue(left, top, /*optional*/width?, /*optional*/height?) {
        this.left = left;
        this.top = top;
        this.width = width || this.width;
        this.height = height || this.height
        this.right = (this.left + this.width);
        this.bottom = (this.top + this.height);
    }

    within(r) {
        return (r.left <= this.left &&
            r.right >= this.right &&
            r.top <= this.top &&
            r.bottom >= this.bottom);
    }

    overlaps(r) {
        return (this.left < r.right &&
            r.left < this.right &&
            this.top < r.bottom &&
            r.top < this.bottom);
    }
}

class Camera {
    AXIS = {
        NONE: "none",
        HORIZONTAL: "horizontal",
        VERTICAL: "vertical",
        BOTH: "both"
    };

    xView: number;
    yView: number;

    xDeadZone: number;
    yDeadZone: number;

    wView: number;
    hView: number;

    axis: string;

    followed: GameObject;

    viewportRect: Rectangle;

    worldRect: Rectangle;


    // Camera constructor
    constructor(xView, yView, canvasWidth, canvasHeight, worldWidth, worldHeight) {
        // position of camera (left-top coordinate)
        this.xView = xView || 0;
        this.yView = yView || 0;

        // distance from followed object to border before camera starts move
        this.xDeadZone = 0; // min distance to horizontal borders
        this.yDeadZone = 0; // min distance to vertical borders

        // viewport dimensions
        this.wView = canvasWidth;
        this.hView = canvasHeight;

        // allow camera to move in vertical and horizontal axis
        this.axis = this.AXIS.BOTH;

        // object that should be followed
        this.followed = null;

        // rectangle that represents the viewport
        this.viewportRect = new Rectangle(this.xView, this.yView, this.wView, this.hView);

        // rectangle that represents the world's boundary (room's boundary)
        this.worldRect = new Rectangle(0, 0, worldWidth, worldHeight);

    }

    follow(gameObject: GameObject, xDeadZone, yDeadZone) {
        this.followed = gameObject;
        this.xDeadZone = xDeadZone;
        this.yDeadZone = yDeadZone;
    }

    update() {
        // keep following the player (or other desired object)
        if (this.followed != null) {
            if (this.axis == this.AXIS.HORIZONTAL || this.axis == this.AXIS.BOTH) {
                // moves camera on horizontal axis based on followed object position
                if (this.followed.gx - this.xView + this.xDeadZone > this.wView)
                    this.xView = this.followed.gx - (this.wView - this.xDeadZone);
                else if (this.followed.gx - this.xDeadZone < this.xView)
                    this.xView = this.followed.gx - this.xDeadZone;

            }
            if (this.axis == this.AXIS.VERTICAL || this.axis == this.AXIS.BOTH) {
                // moves camera on vertical axis based on followed object position
                if (this.followed.gy - this.yView + this.yDeadZone > this.hView)
                    this.yView = this.followed.gy - (this.hView - this.yDeadZone);
                else if (this.followed.gy - this.yDeadZone < this.yView)
                    this.yView = this.followed.gy - this.yDeadZone;
            }

        }

        // update viewportRect
        this.viewportRect.setValue(this.xView, this.yView);

        // don't let camera leaves the world's boundary
        if (!this.viewportRect.within(this.worldRect)) {
            if (this.viewportRect.left < this.worldRect.left)
                this.xView = this.worldRect.left;
            if (this.viewportRect.top < this.worldRect.top)
                this.yView = this.worldRect.top;
            if (this.viewportRect.right > this.worldRect.right)
                this.xView = this.worldRect.right - this.wView;
            if (this.viewportRect.bottom > this.worldRect.bottom)
                this.yView = this.worldRect.bottom - this.hView;
        }

    }
}

class GameObject {
    x: number = 0;
    y: number = 0;
    gx: number = 0;
    gy: number = 0;
    angle: number = 0;
}

class Team {
    constructor(name: string) {
    }
}

class Ship extends GameObject {
    exploied: boolean;
    speed: number = 5;
    hp: number = 10;
    currentHp: number = 10;
    team: Team;

    public cabin: ShipCabin;
    public hull: ShipHull;
    public guns: ShipGunCell[];

    constructor() {
        super();
        this.cabin = new HexShipCabin();
        this.hull = new ShipHull();

        var gun = new ShipGunCell();
        gun.gun = new ShipSimpleGun();
        gun.angleOffset = 0;
        gun.distanceOffset = 7;
        this.guns = [gun];
    }

    draw(context: CanvasRenderingContext2D, xView: number, yView: number) {

        this.hull.draw(context, this.gx - xView, this.gy - yView, this.angle);
        this.cabin.draw(context, this.gx - xView, this.gy - yView, this.angle);

        for (var i = 0; i < this.guns.length; i++)
        {
            var gunCell = this.guns[i];
            var gunX = Math.cos(this.angle + gunCell.angleOffset) * gunCell.distanceOffset;
            var gunY = Math.sin(this.angle + gunCell.angleOffset) * gunCell.distanceOffset;

            gunCell.gun.draw(context, this.gx + gunX - xView, this.gy + gunY - yView, this.angle + gunCell.angle);
        }
    }

    gunFire(gunCells: ShipGunCell[]): Bullet[] {
        var bullets = [];
        for (var i = 0; i < this.guns.length; i++) {
            var gunCell = this.guns[i];
            var gunX = Math.cos(this.angle + gunCell.angleOffset) * gunCell.distanceOffset + this.gx;
            var gunY = Math.sin(this.angle + gunCell.angleOffset) * gunCell.distanceOffset + this.gy;

            gunCell.reloadCurrent = 0;

            var gunCellBullets = gunCell.gun.generateBullets(gunX, gunY, this.angle + this.guns[i].angle, this.team);

            for (var j = 0; j < gunCellBullets.length; j++) {
                bullets.push(gunCellBullets[j]);
            }
        }

        return bullets;
    }
}

class ShipGunCell {
    public gun: ShipGun;
    public angleOffset: number;
    public distanceOffset: number;
    public angle: number = 0;
    reload: number = 100;
    reloadCurrent: number = 10;

    drawGui(context: CanvasRenderingContext2D, x: number, y: number)
    {
        context.save();

        if (this.reloadCurrent < this.reload) {
            context.beginPath();
            context.rect(x, y, 32, 32);
            context.fillStyle = "rgba(255,0,0, 0.3)";
            context.fill();
        }

        context.beginPath();
        context.rect(x, y, 32, this.reloadCurrent / this.reload * 32);
        context.fillStyle = "rgba(255,255,255, 0.3)";
        context.fill();

        context.beginPath();
        context.rect(x, y, 32, 32);
        context.strokeStyle = "white";
        context.lineWidth = 2;
        context.stroke();

        this.gun.draw(context, x + 16, y + 16, 0);

        context.restore();
    }
}

class PlayerShip extends Ship {
    update() {
        for (var i = 0; i < this.guns.length; i++) {
            var gunCell = this.guns[i];
            if (gunCell.reloadCurrent < gunCell.reload) {
                gunCell.reloadCurrent++;
            }
        }
    }
}

class MobShip extends Ship {
    targetX: number = null;
    targetY: number = null;
    mindCounter: number = 0;

    public cabin: ShipCabin;
    public hull: ShipHull;

    constructor() {
        super();
        this.cabin = new SphereShipCabin();
        this.hull = new ShipHull();
    }

    update(playerShip: Ship): Bullet[] {
        //var distance = Math.sqrt((this.x - playerShip.x) * (this.x - playerShip.y) + (this.y - playerShip.y) * (this.y - playerShip.y))
        /*if (distance < 300)*/ {
            var targetAngle = MathUtils.LertAngle(this.x, this.y, playerShip.x, playerShip.y, this.angle, 0.05);
            this.angle = targetAngle;
        /*}
        else {*/

            /*if (this.targetX != null && this.targetY != null) {
                var targetAngle = MathUtils.LertAngle(this.targetX, this.x, this.targetY, this.y, this.angle, 0.05);
                this.angle = targetAngle;

                var distance = Math.sqrt((this.x - this.targetX) * (this.x - this.targetX) + (this.y - this.targetY) * (this.y - this.targetY))

                if (distance > 10) {

                    
                }
                else {
                    this.targetX = null;
                    this.targetY = null;
                }
            }
            else {
                this.mindCounter++;
                if (this.mindCounter > 10) {
                    var roll = Math.random();
                    if (roll > 0.5) {
                        this.targetX = Math.random() * 5000;
                        this.targetY = Math.random() * 3000;
                    }

                    this.mindCounter = 0;
                }
            }*/
        }

        this.x += Math.cos(this.angle) * this.speed;
        this.y += Math.sin(this.angle) * this.speed;

        for (var i = 0; i < this.guns.length; i++) {
            var gunCell = this.guns[i];
            if (gunCell.reloadCurrent < gunCell.reload) {
                gunCell.reloadCurrent++;
            }
            else {
                gunCell.reloadCurrent = 0;
                var bullets = this.gunFire(this.guns);
                return bullets;
            }
        }

        return null;
    }
}

interface IShipPart {
    draw(context: CanvasRenderingContext2D, x: number, y: number, a: number);
}

class ShipHull implements IShipPart {
    draw(context: CanvasRenderingContext2D, x: number, y: number, a: number)
    {
        context.save();

        context.beginPath();
        context.moveTo(Math.cos(a) * 10 + x, Math.sin(a) * 10 + y);
        context.lineTo(Math.cos(a - (Math.PI + Math.PI / 10)) * 15 + x, Math.sin(a - (Math.PI + Math.PI / 10)) * 15 + y);
        context.lineTo(Math.cos(a + (Math.PI + Math.PI / 10)) * 15 + x, Math.sin(a + (Math.PI + Math.PI / 10)) * 15 + y);
        context.lineTo(Math.cos(a) * 10 + x, Math.sin(a) * 10 + y);

        context.fillStyle = '#8ED6FF';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = 'black';
        context.stroke();

        context.restore();
    }
}

abstract class ShipCabin implements IShipPart {
    abstract draw(context: CanvasRenderingContext2D, x: number, y: number, a: number);
}

abstract class ShipGun implements IShipPart {
    abstract draw(context: CanvasRenderingContext2D, x: number, y: number, a: number): void;

    abstract generateBullets(worldX: number, worldY: number, worldAngle: number, team: Team): Bullet[];
}

class HexShipCabin extends ShipCabin {
    draw(context: CanvasRenderingContext2D, x: number, y: number, a: number): void {

        context.save();

        context.beginPath();
        context.moveTo(x, y);
        for (var i = 0; i < 7; i++) {
            var a1 = (Math.PI * 2) * i / 6;
            context.lineTo(Math.cos(a + a1) * 10 + x, Math.sin(a + a1) * 10 + y);
        }
        context.fillStyle = '#886666';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = 'black';
        context.stroke();

        context.beginPath();
        context.moveTo(x, y);
        for (var i = 0; i < 7; i++) {
            var a1 = (Math.PI * 2) * i / 6;
            context.lineTo(Math.cos(a + a1) * 7.5 + x, Math.sin(a + a1) * 7.5 + y);
        }
        context.fillStyle = '#DDDDDD';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = 'black';
        context.stroke();

        context.restore();
    }
}

class SphereShipCabin extends ShipCabin {
    draw(context: CanvasRenderingContext2D, x: number, y: number, a: number) {

        context.save();

        context.beginPath();
        context.arc(x, y, 10, 0, 2 * Math.PI, false);
        context.fillStyle = '#886666';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = '#060';
        context.stroke();

        context.beginPath();
        context.arc(x, y, 7, 0, 2 * Math.PI, false);
        context.fillStyle = '#DDDDDD';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = '#060';
        context.stroke();

        context.restore();
    }
}

class ShipSimpleGun extends ShipGun {
    draw(context: CanvasRenderingContext2D, x: number, y: number, a: number) {
        context.save();

        context.beginPath();
        context.moveTo(Math.cos(a) * 7 + x, Math.sin(a) * 7 + y);
        context.lineTo(Math.cos(a - (Math.PI + Math.PI / 10)) * 7 + x, Math.sin(a - (Math.PI + Math.PI / 10)) * 7 + y);
        context.lineTo(Math.cos(a + (Math.PI + Math.PI / 10)) * 7 + x, Math.sin(a + (Math.PI + Math.PI / 10)) * 7 + y);
        context.lineTo(Math.cos(a) * 7 + x, Math.sin(a) * 7 + y);

        context.fillStyle = 'gray';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = 'black';
        context.stroke();

        context.restore();
    }

    generateBullets(worldX: number, worldY: number, worldAngle: number, team: Team): Bullet[] {
        var bullets = [];

        var bullet = new Bullet();
        bullet.x = worldX;
        bullet.y = worldY;
        bullet.gx = bullet.x;
        bullet.gy = bullet.y;
        bullet.angle = worldAngle;
        bullet.team = team;

        bullets.push(bullet);

        return bullets;
    }
}

class ShipSimpleGun2 extends ShipGun {
    draw(context: CanvasRenderingContext2D, x: number, y: number, a: number) {
        context.save();

        context.beginPath();
        context.moveTo(Math.cos(a) * 10 + x, Math.sin(a) * 10 + y);
        context.lineTo(Math.cos(a - (Math.PI + Math.PI / 10)) * 4 + x, Math.sin(a - (Math.PI + Math.PI / 10)) * 4 + y);
        context.lineTo(Math.cos(a + (Math.PI + Math.PI / 10)) * 4 + x, Math.sin(a + (Math.PI + Math.PI / 10)) * 4 + y);
        context.lineTo(Math.cos(a) * 10 + x, Math.sin(a) * 10 + y);

        context.fillStyle = 'red';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = 'black';
        context.stroke();

        context.restore();
    }

    generateBullets(worldX: number, worldY: number, worldAngle: number, team: Team): Bullet[] {
        var bullets = [];

        for (var a = -Math.PI / 2; a < Math.PI / 2; a += Math.PI) {

            var x = Math.cos(worldAngle + a) * 3 + worldX;
            var y = Math.sin(worldAngle + a) * 3 + worldY;

            var bullet = new Bullet();
            bullet.x = x;
            bullet.y = y;
            bullet.gx = bullet.x;
            bullet.gy = bullet.y;
            bullet.angle = worldAngle;
            bullet.team = team;

            bullets.push(bullet);
        }

        return bullets;
    }
}

class Obstacle extends GameObject {
    parts: any[];

    constructor() {
        super();
        this.parts = [];
        var partCount = 5 + Math.floor(Math.random() * 4);
        for (var i = 0; i < partCount; i++) {
            var a = Math.random() * Math.PI * 2;
            var d = 3 + Math.random() * 4;
            this.parts.push({
                x: Math.cos(a) * d,
                y: Math.sin(a) * d
            });
        }
    }

    draw(context: CanvasRenderingContext2D, xView: number, yView: number) {

        context.save();

        context.beginPath();
        context.arc(this.gx - xView, this.gy - yView, 7, 0, 2 * Math.PI, false);
        context.fillStyle = '#333333';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = '#eeaaaa';
        context.stroke();

        for (var j = 0; j < this.parts.length; j++) {
            context.beginPath();
            context.arc(this.parts[j].x + this.gx - xView, this.parts[j].y + this.gy - yView, 4, 0, 2 * Math.PI, false);
            context.fillStyle = '#555555';
            context.fill();
            context.lineWidth = 1;
            context.strokeStyle = '#eeaaaa';
            context.stroke();
        }

        context.restore();
    }
}

class BackgroundObject extends GameObject {

}

class Star extends BackgroundObject {
    private size: number;

    constructor() {
        super();
        this.size = Math.random() * 1.5 + 0.5;
    }

    draw(context: CanvasRenderingContext2D, xView: number, yView: number) {
        context.save();

        context.beginPath();
        context.arc(this.gx - xView, this.gy - yView, this.size, 0, 2 * Math.PI, false);
        context.lineWidth = 1;
        context.strokeStyle = '#ffffaa';
        context.stroke();

        context.restore();
    }
}

class SpaceSector extends GameObject {
    draw(context: CanvasRenderingContext2D, xView: number, yView: number) {
       // context.save();

        context.beginPath();
        context.fillRect(0 - xView, 0 - yView, 5000, 3000);
        context.fillStyle = '#000033';
        context.fill();

        context.beginPath();
        context.rect(0 - xView, 0 - yView, 5000, 3000);
        context.strokeStyle = '#555';
        context.stroke();

        //context.restore();
    }
}

class Bullet extends GameObject {
    angle: number;
    speed: number = 10;
    exploied: boolean;
    lifetime: number = 0;
    team: Team;
    damage: number = 1;
    lastX: number;
    lastY: number;

    draw(context: CanvasRenderingContext2D, xView: number, yView: number) {
        context.save();

        context.beginPath();
        context.arc(this.gx - xView, this.gy - yView, 4, 0, 2 * Math.PI, false);
        context.fillStyle = 'yellow';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = '#559966';
        context.stroke();

        context.restore();
    }

    update() {
        if (this.lifetime < 100) {
            this.lifetime++;
            this.lastX = this.x;
            this.lastY = this.y;
            this.x = this.x + Math.cos(this.angle) * this.speed;
            this.y = this.y + Math.sin(this.angle) * this.speed;
        }
        else {
            this.exploied = true;
        }
    }

    checkCollision(persons: Ship[]) {

    }
}

class ShotMarker extends GameObject {
    public size: number;

    draw(context: CanvasRenderingContext2D, xView: number, yView: number) {
        context.save();

        context.beginPath();
        context.arc(this.gx - xView, this.gy - yView, this.size, 0, 2 * Math.PI, false);
        context.lineWidth = 1;
        context.strokeStyle = '#ffffaa';
        context.stroke();
        context.fillStyle = 'rgba(255, 255, 255, 0.3)'
        context.fill();

        context.restore();
    }
}

class Resource {
    constructor(public sid: string, public name: string, public count: number = 0) {

    }

    draw(context: CanvasRenderingContext2D, x: number, y: number) {
        context.save();

        context.fillStyle = 'rgba(255, 255, 255, 1)';
        context.fillText(this.name, x, y+10);

        context.beginPath();
        context.arc(x, y + 20, 6, 0, 2 * Math.PI, false);
        context.lineWidth = 1;
        context.strokeStyle = '#ffffaa';
        context.stroke();
        context.fillStyle = 'rgba(255, 255, 255, 0.3)';
        context.fill();

        context.fillStyle = 'rgba(255, 255, 255, 1)';
        context.fillText("x " + this.count, x + 15, y + 20);

        context.restore();
    }
}

class MainApp {
    mainCanvas: HTMLCanvasElement;
    drawingContext: CanvasRenderingContext2D;
    renderTime: number;
    logicTime: number;

    isMovingLeft: boolean = false;
    isMovingRight: boolean = false;
    isMovingUp: boolean = false;
    isMovingDown: boolean = false;
    isShooting: boolean = false;
    shootingX: number;
    shootingY: number;

    teams: Team[];
    player: PlayerShip;
    currentSpaceSector: SpaceSector;
    camera: Camera;
    bullets: Bullet[] = [];
    mobs: MobShip[] = [];
    obstacles: Obstacle[] = [];
    stars: Star[] = [];
    shotMarker: ShotMarker;

    resources: Resource[];

    constructor(element: HTMLElement) {
        var width = document.body.clientWidth;
        var height = document.body.clientHeight;

        this.mainCanvas = <HTMLCanvasElement>document.getElementById("main-canvas");

        this.mainCanvas.width = width;
        this.mainCanvas.height = height;
        this.drawingContext = this.mainCanvas.getContext("2d");
        this.renderTime = new Date().getTime();
        this.logicTime = new Date().getTime();

        this.teams = [new Team("player"), new Team("mobs")];

        this.player = new PlayerShip();
        this.player.speed += this.player.speed / 2;
        this.player.team = this.teams[0];
        this.currentSpaceSector = new SpaceSector();
        this.camera = new Camera(0, 0, this.mainCanvas.width, this.mainCanvas.height, 5000, 3000);
        this.camera.follow(this.player, this.mainCanvas.width / 2, this.mainCanvas.height / 2);

        this.shotMarker = new ShotMarker();
        this.shotMarker.size = 20;

        this.resources = [new Resource("fire-brick", "fire brick", 0), new Resource("steel-dust", "steel dust", 0)];

        for (var i = 0; i < 50; i++)
        {
            var mob = new MobShip();
            mob.x = Math.random() * 5000;
            mob.y = Math.random() * 3000;
            mob.gx = mob.x;
            mob.gy = mob.y;
            mob.angle = Math.random() * Math.PI * 2;
            mob.team = this.teams[1];
            this.mobs.push(mob);
        }

        for (var i = 0; i < 100; i++)
        {
            var rock = new Obstacle();
            rock.x = Math.random() * 5000;
            rock.y = Math.random() * 3000;
            rock.gx = rock.x;
            rock.gy = rock.y;
            rock.angle = Math.random() * Math.PI * 2;
            this.obstacles.push(rock);
        }

        for (var i = 0; i < 200; i++) {
            var star = new Star();
            star.x = Math.random() * 5000;
            star.y = Math.random() * 3000;
            star.gx = star.x;
            star.gy = star.y;
            this.stars.push(star);
        }
    }

    render() {
        requestAnimationFrame(() => {
            // clear
            this.drawingContext.clearRect(0, 0, this.mainCanvas.width, this.mainCanvas.height);
            var currentTime = new Date().getTime();
            this.renderFrame((currentTime - this.renderTime) / 1000);
            this.renderTime = currentTime;
            this.render();
        });
    }

    startLogicLoop() {
        window.setInterval(() => {
            var currentTime = new Date().getTime();
            this.updateLogic((currentTime - this.logicTime) / 1000);
            this.logicTime = currentTime;
        }, 100);
    }

    start() {
        this.startLogicLoop();
        this.render();
    }

    updateLogic(deltaTime: number) {
        const playerRotateSpeed: number = 0.05;
        if (this.isMovingLeft) {
            this.player.angle -= playerRotateSpeed;
            if (this.player.angle < 0)
            {
                this.player.angle = Math.PI * 2 - playerRotateSpeed;
            }
        }
        else if (this.isMovingRight) {
            this.player.angle += playerRotateSpeed;

            if (this.player.angle >= Math.PI * 2) {
                this.player.angle = playerRotateSpeed;
            }
        }

        //if (this.isMovingUp) {
        //    this.player.y -= this.s;
        //}
        //else if (this.isMovingDown) {
        //    this.player.y += this.s;
        //}

        this.player.update();

        this.player.x += Math.cos(this.player.angle) * this.player.speed;
        this.player.y += Math.sin(this.player.angle) * this.player.speed;

        if (this.isShooting)
        {
            var gunCells = this.player.guns.filter(x => x.reloadCurrent >= x.reload);

            if (gunCells.length > 0) {

                var bullets = this.player.gunFire(gunCells);

                for (var j = 0; j < bullets.length; j++) {
                    this.bullets.push(bullets[j]);
                }
            }
        }

        // Обновляем мобов
        for (var i = 0; i < this.mobs.length; i++)
        {
            var mob = this.mobs[i];
            var bullets = mob.update(this.player);
            if (bullets != null)
            {
                for (var j = 0; j < bullets.length; j++) {
                    this.bullets.push(bullets[j]);
                }
            }
        }

        // обновляем снаряды

        for (var i = 0; i < this.bullets.length; i++)
        {
            var bullet = this.bullets[i];
            bullet.update();

            if (bullet.team == this.teams[0]) {

                // проверяем столкновения с мобами
                for (var j = 0; j < this.mobs.length; j++) {
                    var mob = this.mobs[j];

                    if (this.distance(mob, bullet) <= 10) {
                        bullet.exploied = true;
                        mob.exploied = true;

                        // начисляем ресурсы
                        var resourceRoll = Math.floor((Math.random() * (this.resources.length - 1) * 100) / 100);
                        this.resources[resourceRoll].count++;
                    }
                }
            }
            else if (bullet.team == this.teams[1])
            {
                if (this.distance(this.player, bullet) <= 10) {
                    bullet.exploied = true;
                    this.player.exploied = true;
                }
            }
        }

        var mi = 0;
        while (mi < this.mobs.length)
        {
            var mob = this.mobs[mi];
            if (mob.exploied) {
                this.mobs.splice(mi, 1);
            }
            else {
                mi++;
            }
        }

        var bi = 0;
        while (bi < this.bullets.length) {
            var bullet = this.bullets[bi];
            if (bullet.exploied) {
                this.bullets.splice(bi, 1);
            }
            else {
                bi++;
            }
        }

        this.shotMarker.size = this.distance(this.player, this.shotMarker) / 10;
    }

    renderFrame(deltaTime: number) {
        this.player.gx = this.lerp(this.player.gx, this.player.x, deltaTime * 10);
        this.player.gy = this.lerp(this.player.gy, this.player.y, deltaTime * 10);
        this.camera.update();

        this.currentSpaceSector.draw(this.drawingContext, this.camera.xView, this.camera.yView);
        for (var i = 0; i < this.stars.length; i++)
        {
            this.stars[i].draw(this.drawingContext, this.camera.xView, this.camera.yView);
        }

        this.player.draw(this.drawingContext, this.camera.xView, this.camera.yView);

        for (var i = 0; i < this.mobs.length; i++)
        {
            var mob = this.mobs[i];
            mob.gx = this.lerp(mob.gx, mob.x, deltaTime * 10);
            mob.gy = this.lerp(mob.gy, mob.y, deltaTime * 10);
            mob.draw(this.drawingContext, this.camera.xView, this.camera.yView);
        }

        for (var i = 0; i < this.bullets.length; i++)
        {
            var bullet = this.bullets[i];
            bullet.gx = this.lerp(bullet.gx, bullet.x, deltaTime * 10);
            bullet.gy = this.lerp(bullet.gy, bullet.y, deltaTime * 10);
            bullet.draw(this.drawingContext, this.camera.xView, this.camera.yView);
        }

        for (var i = 0; i < this.obstacles.length; i++)
        {
            var rock = this.obstacles[i];
            rock.draw(this.drawingContext, this.camera.xView, this.camera.yView);
        }


        /*this.shotMarker.gx = this.lerp(this.shotMarker.gx, this.shotMarker.x, deltaTime * 10);
        this.shotMarker.gy = this.lerp(this.shotMarker.gy, this.shotMarker.y, deltaTime * 10);
        this.shotMarker.draw(this.drawingContext, this.camera.xView, this.camera.yView);*/

        for (var i = 0; i < this.resources.length; i++)
        {
            this.resources[i].draw(this.drawingContext, 10, i * 30);
        }

        for (var i = 0; i < this.player.guns.length; i++)
        {
            this.player.guns[i].drawGui(this.drawingContext, 200, i * 30 + 5);
        }
    }

    lerp(a, b, t) {
        var d = b - a;
        return a + d * t;
    }

    distance(a: GameObject, b: GameObject)
    {
        return Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }
}

class MathUtils {
    public static LertAngle(x1: number, y1: number, x2: number, y2: number, angle: number, easing: number) {
        var dx = x2 - x1;
        var dy = y2 - y1;
        var len = Math.sqrt(dx * dx + dy * dy);
        dx /= len ? len : 1.0;
        dy /= len ? len : 1.0;

        // get current direction
        var dirx = Math.cos(angle);
        var diry = Math.sin(angle);
        // ease the current direction to the target direction
        dirx += (dx - dirx) * easing;
        diry += (dy - diry) * easing;

        return Math.atan2(diry, dirx);
    }
}

window.onload = () => {
    var el = document.getElementById('content');
    var app = new MainApp(el);
    app.start();

    document.onkeydown = function (e) {
        var charCode = (typeof e.which == "number") ? e.which : e.keyCode
        if (charCode == 65) {
            app.isMovingRight = false;
            app.isMovingLeft = true;
        }
        else if (charCode == 68) {
            app.isMovingLeft = false;
            app.isMovingRight = true;
        }

        if (charCode == 87) {
            app.isMovingUp = true;
            app.isMovingDown = false;
        }
        else if (charCode == 83) {
            app.isMovingUp = false;
            app.isMovingDown = true;
        }

        if (charCode == 32) {
            app.isShooting = true;
        }

        console.log(charCode);
    };

    document.onkeyup = function (e) {
        var charCode = (typeof e.which == "number") ? e.which : e.keyCode
        if (charCode == 65) {
            app.isMovingRight = false;
            app.isMovingLeft = false;
        }
        else if (charCode == 68) {
            app.isMovingLeft = false;
            app.isMovingRight = false;
        }

        if (charCode == 87) {
            app.isMovingUp = false;
            app.isMovingDown = false;
        }
        else if (charCode == 83) {
            app.isMovingUp = false;
            app.isMovingDown = false;
        }

        if (charCode == 32)
        {
            app.isShooting = false;
        }
    };

    document.onmousedown = (e) => {
        app.isShooting = true;
        
    }

    document.onmouseup = (e) => {
        app.isShooting = false;
    }

    document.onmousemove = (e) => {
        var worldX = e.clientX + app.camera.xView;
        var worldY = e.clientY + app.camera.yView;
        var worldAngle = Math.atan2(worldY - app.player.y, worldX - app.player.x);
        var shipVectorX = Math.cos(app.player.angle);
        var shipVectorY = Math.sin(app.player.angle);
        var shipAngle = Math.atan2(shipVectorY, shipVectorX);
        var gunAngle = worldAngle - shipAngle;

        for (var i = 0; i < app.player.guns.length; i++)
        {
            var gunCell = app.player.guns[i];
            if (gunAngle < -Math.PI / 6)
            {
                gunAngle = -Math.PI / 6;
            }

            if (gunAngle > Math.PI / 6) {
                gunAngle = Math.PI / 6;
            }

            gunCell.angle = gunAngle;
        }

        app.shotMarker.x = worldX;
        app.shotMarker.y = worldY;
    }

    document.getElementById("update-weapon").onclick = (event) => {
        app.player.guns[0].gun = new ShipSimpleGun2();
    };
};
